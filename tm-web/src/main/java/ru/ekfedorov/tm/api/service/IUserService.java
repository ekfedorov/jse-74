package ru.ekfedorov.tm.api.service;

import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.ekfedorov.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    List<User> findAll();

    User add(final User entity);

    void addAll(final Collection<User> collection);

    User findById(final String id);

    void clear();

    void remove(final User entity);

    void removeById(final String id);

    User findByLogin(final String login);

    User findByEmail(final String email);

    void removeByLogin(final String login);

    boolean isLoginExist(final String login);

    boolean isEmailExist(final String email);

    User updateUser(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    );

    User lockByLogin(final String login);

    User unlockByLogin(final String login);

}
