package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IRecordService<E extends AbstractEntity> {

    List<E> findAll();

    E add(final E entity);

    void addAll(final Collection<E> collection);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void remove(final E entity);

}