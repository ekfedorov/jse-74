package ru.ekfedorov.tm.exception;

import org.jetbrains.annotations.NotNull;

public class EmptyNameException extends AbstractException {

    @NotNull
    public EmptyNameException() {
        super("Error. Name is empty");
    }

}
