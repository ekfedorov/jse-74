package ru.ekfedorov.tm.exception.system;

import ru.ekfedorov.tm.exception.AbstractException;

public final class LoginExistException extends AbstractException {

    public LoginExistException() {
        super("Warning! Login already exists...");
    }

}
