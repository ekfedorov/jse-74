package ru.ekfedorov.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ekfedorov.tm.api.service.IReceiverService;
import ru.ekfedorov.tm.cofiguration.LoggerConfiguration;
import ru.ekfedorov.tm.listener.LogMessageListener;
import ru.ekfedorov.tm.service.ActiveMQConnectionService;

public class Application {

    @SneakyThrows
    public static void main(final String[] args) {
        @NotNull AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final IReceiverService receiverService = context.getBean(ActiveMQConnectionService.class);
        receiverService.receive(context.getBean(LogMessageListener.class));
    }

}