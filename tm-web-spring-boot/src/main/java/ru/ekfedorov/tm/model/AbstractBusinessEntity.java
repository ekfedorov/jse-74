package ru.ekfedorov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.ekfedorov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractBusinessEntity extends AbstractEntity{

    protected Date created = new Date();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateFinish;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateStart;

    protected String description = "";

    protected String name = "";

    protected Status status = Status.NOT_STARTED;

    @Column(name = "user_id")
    protected String userId;

}

