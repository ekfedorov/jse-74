package ru.ekfedorov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.api.service.ITaskService;
import ru.ekfedorov.tm.model.AuthorizedUser;
import ru.ekfedorov.tm.model.Project;

import java.util.List;

@Controller
public class TasksController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Secured({"ROLE_USER"})
    @GetMapping("/tasks")
    public ModelAndView index(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user
    ) {
        return new ModelAndView("task-list", "tasks", taskService.findAll(user.getUserId()));
    }

    @ModelAttribute("view_name")
    public String getViewName() {
        return "TASKS";
    }

    @ModelAttribute("projects")
    public List<Project> getProjects(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user
    ) {
        return projectService.findAll(user.getUserId());
    }

}
